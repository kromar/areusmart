package pl.parser.nbp.financial;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by makrolik on 1/10/14.
 */
public class Rates {

    private List<Double> rates;
    private double avg = -1;

    private Rates(){
        rates = new LinkedList<Double>();
    }

    public static <T> Rates asNumbers(List<T> srates){
        Rates rates = new Rates();
        for (T srate: srates){
            String r = (String) srate;
            if (r.contains(",")){
                r = r.replace(",",".");
            }
            rates.rates.add(Double.parseDouble(r));
        }
        return rates;
    }

    public double avg(){
        double acc = 0;
        for (double r: rates){
            acc += r;
        }
        avg = Math.round(acc/rates.size()*10000.0)/10000.0;
        return avg;
    }

    public double std(){
        double acc = 0.0;
        int size = rates.size();
        if (avg <= -1){
            return 0;
        }
        for (double rate: rates){
            acc += Math.pow((avg - rate), 2);
        }
        return Math.round(Math.sqrt(acc/size)*10000.0)/10000.0;
    }

    public List<Double> getValues() {
        return rates;
    }
}

package pl.parser.nbp.utils;

import java.util.Arrays;

/**
 * Created by Rainman on 09.01.14.
 */
public class Matchers {

    public static Boolean isDate(String date){
        String regex = "\\d{4}-\\d{2}-\\d{2}";
        return date.matches(regex);
    }

    public static String yymmdd(String date){
        Arrays.asList(1,2);
        return date.replace("-", "").substring(2);
    }

}

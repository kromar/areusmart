package pl.parser.nbp.utils;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by Rainman on 09.01.14.
 */
public class UrlConntent {

    public static List<String> get(String url_text){

        List<String> lines = null;

        try {
            URL url = new URL(url_text);
            URLConnection connection = url.openConnection();
            lines = IOUtils.readLines(connection.getInputStream());

        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            return lines;
        }
    }
}

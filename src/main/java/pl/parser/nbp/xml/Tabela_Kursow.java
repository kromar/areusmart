package pl.parser.nbp.xml;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Rainman on 09.01.14.
 */
@Root
public class Tabela_Kursow {

    @Element
    private String numer_tabeli;
    @Element
    private String data_notowania;
    @Element
    private String data_publikacji;
    @ElementList(name = "pozycja", inline = true)
    private List<Pozycja> pozycja;

    @Attribute
    private String typ;
    @Attribute
    private String uid;

    public String getNumer_tabeli() {
        return numer_tabeli;
    }

    public String getData_notowania() {
        return data_notowania;
    }

    public String getData_publikacji() {
        return data_publikacji;
    }

    public List<Pozycja> getPozycja() {
        return pozycja;
    }

}

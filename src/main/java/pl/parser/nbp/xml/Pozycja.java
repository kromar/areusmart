package pl.parser.nbp.xml;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Created by Rainman on 09.01.14.
 */
@Root
public class Pozycja {

    @Element
    private String nazwa_waluty;
    @Element
    private String przelicznik;
    @Element
    private String kod_waluty;
    @Element
    private String kurs_kupna;
    @Element
    private String kurs_sprzedazy;

    public String getNazwa_waluty() {
        return nazwa_waluty;
    }

    public String getPrzelicznik() {
        return przelicznik;
    }

    public String getKod_waluty() {
        return kod_waluty;
    }

    public String getKurs_kupna() {
        return kurs_kupna;
    }

    public String getKurs_sprzedazy() {
        return kurs_sprzedazy;
    }
}

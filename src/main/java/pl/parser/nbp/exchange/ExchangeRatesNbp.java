package pl.parser.nbp.exchange;

import com.google.common.base.Joiner;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import pl.parser.nbp.utils.UrlConntent;
import pl.parser.nbp.date.TimeFrame;
import pl.parser.nbp.xml.Pozycja;
import pl.parser.nbp.xml.Tabela_Kursow;

import static pl.parser.nbp.utils.Matchers.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Rainman on 09.01.14.
 */
public class ExchangeRatesNbp implements ExchangeRates {

    private final String URL = "http://www.nbp.pl/kursy/xml/";
    private final String DIR = "dir.txt";
    private List<String> fileNames;
    private List<Tabela_Kursow> exchangeTables;
    private List<String> rates;


    public ExchangeRatesNbp(){
        this.fileNames = new LinkedList<String>();
        this.exchangeTables = new LinkedList<Tabela_Kursow>();
        this.rates = new LinkedList<String>();
    }


    @Override
    public ExchangeRatesNbp fetchFileNames(String fromDate, String toDate) {
        if (!(isDate(fromDate) && isDate(toDate))){
            throw new IllegalArgumentException("Provided dates are not in acceptable format YYYY-MM-DD");
        }

        TimeFrame timeFrame = TimeFrame.builder()
                .fromDate(yymmdd(fromDate))
                .toDate(yymmdd(toDate))
                .build();

        List<String> lines = UrlConntent.get(URL + DIR);
        for (String line: lines){
            if (timeFrame.belongs(line.substring(5)) && line.charAt(0) == 'c'){
                fileNames.add(line + ".xml");
                if (fileNames.size() >= timeFrame.length()){
                    break;
                }
            }
        }
        return this;
    }


    @Override
    public ExchangeRatesNbp fetchExchangeTables() {

        for (String fileName: fileNames){
            List<String> lines = UrlConntent.get(URL + fileName);
            String source = Joiner.on("\n").join(lines);
            Serializer serializer = new Persister();
            try {
                Tabela_Kursow exchangeTable = serializer.read(Tabela_Kursow.class, source);
                exchangeTables.add(exchangeTable);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return this;
    }


    @Override
    public List<String> fetchRates(String currency) {
        for (Tabela_Kursow echangeTable: exchangeTables){
            for (Pozycja position: echangeTable.getPozycja()){
                if (position.getKod_waluty().equals(currency)){
                    rates.add(position.getKurs_kupna());
                }
            }
        }
        return rates;
    }

    public List<String> getFileNames() {
        return fileNames;
    }

    public List<Tabela_Kursow> getExchangeTables() {
        return exchangeTables;
    }

    public List<String> getRates() {
        return rates;
    }
}

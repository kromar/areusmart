package pl.parser.nbp.exchange;

import java.util.List;

/**
 * Created by Rainman on 09.01.14.
 */
public interface ExchangeRates {

    public <T> T fetchFileNames(String fromDate, String toDate);

    public <T> List<T> fetchRates(String currency);

    public <T> T fetchExchangeTables();
}

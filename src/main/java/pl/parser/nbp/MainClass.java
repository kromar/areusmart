package pl.parser.nbp;

import pl.parser.nbp.exchange.ExchangeRatesNbp;
import pl.parser.nbp.financial.Rates;
import pl.parser.nbp.xml.Tabela_Kursow;

import java.util.List;

/**
 * Created by Rainman on 09.01.14.
 */
public class MainClass {

    public static void main(String[] args){
        ExchangeRatesNbp exchangeRatesNbp = new ExchangeRatesNbp();
        List<String> stringRates = exchangeRatesNbp
                .fetchFileNames("2013-01-28", "2013-01-31")
                .fetchExchangeTables()
                .fetchRates("USD");

        Rates rates = Rates.asNumbers(stringRates);

        System.out.println(rates.avg());
        System.out.println(rates.std());

    }
}

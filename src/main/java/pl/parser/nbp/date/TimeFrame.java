package pl.parser.nbp.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Rainman on 09.01.14.
 */
public class TimeFrame {

    private Calendar fromDate;
    private Calendar toDate;
    private final SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");

    private TimeFrame(){}

    public static class Builder{

        private TimeFrame timeFrame;


        public Builder(){
            this.timeFrame = new TimeFrame();
            this.timeFrame.setFromDate(Calendar.getInstance());
            this.timeFrame.setToDate(Calendar.getInstance());
        }

        public Builder fromDate(String from){
            Calendar cal = stringToCalendar(from);
            cal.add(Calendar.DATE, -1);
            this.timeFrame.setFromDate(cal);
            return this;
        }

        public Builder toDate(String to){
            Calendar cal = stringToCalendar(to);
            cal.add(Calendar.DATE, 1);
            this.timeFrame.setToDate(cal);
            return this;
        }

        public TimeFrame build(){
            return this.timeFrame;
        }

        private Calendar stringToCalendar(String date) {
            Calendar calendar = Calendar.getInstance();

            try {
                calendar.setTime(this.timeFrame.formatter.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return calendar;
        }
    }

    public static Builder builder(){
        return new Builder();
    }

    public Boolean belongs(String d){
        Boolean within = false;
        Calendar date = Calendar.getInstance();

        try {
            date.setTime(formatter.parse(d));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date.after(fromDate) && date.before(toDate)){
            within = true;
        }

        return within;
    }

    public int length(){
        long difference = this.toDate.getTimeInMillis() - this.fromDate.getTimeInMillis();
        return (int)(difference/3600000 - 24)/24;
    }


    private void setFromDate(Calendar fromDate) {


        this.fromDate = fromDate;
    }

    private void setToDate(Calendar toDate) {
        this.toDate = toDate;
    }

    public String getFromDate() {
        return formatter.format(fromDate.getTime());
    }

    public String getToDate() {
        return formatter.format(toDate.getTime());
    }
}

/**
 * Created by makrolik on 1/11/14.
 */

import org.junit.Before;
import org.junit.Test;
import pl.parser.nbp.financial.Rates;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

public class FinanceTest {

    private List<String> stringRates;
    private Rates rates;

    @Before
    public void setUp(){
        stringRates = Arrays.asList("3,0000", "4,0000", "5,0000");
        rates = Rates.asNumbers(stringRates);
    }

    @Test
    public void testStaticFactoryMethodNumberOfRates(){
        assertThat(rates.getValues(), hasSize(equalTo(3)));
    }

    @Test
    public void testStaticFactoryMethodRatesValues(){
        assertThat(rates.getValues(), contains(equalTo(3.0000), equalTo(4.0000), equalTo(5.0000)));
    }

    @Test
    public void testFinanceAverageMethod(){
        assertThat(rates.avg(), is(4.0));
    }
}
